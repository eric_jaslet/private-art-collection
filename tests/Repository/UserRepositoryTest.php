<?php

namespace App\Tests\Repository;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    public function testUserCount()
    {
        self::bootKernel();
        $container = static::getContainer();
        $repo = $container->get(UserRepository::class);
        $users = $repo->count([]);
        $this->assertEquals(10, $users);
    }
}