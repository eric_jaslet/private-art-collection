<?php

namespace App\Controller\FrontOffice;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class LoginControllerTest extends WebTestCase
{
    public function testLoginPageResponse()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testFormExist()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $this->assertSelectorExists('input[name=_csrf_token]');
        $this->assertSelectorExists('input[name=_username]');
        $this->assertSelectorExists('input[name=_password]');
        $this->assertSelectorExists('button[type=submit]');
    }

    // out memory
    public function testBadCredential()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('login')->form([
            '_username' => 'test',
            '_password'  => '123',

        ]);
        $client->submit($form);
        // $this->assertResponseRedirects(); // boucle multiple redirect
        $client->followRedirect();
        $this->assertSelectorExists('.form-message-error');
    }

    public function testSuccessCredential()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('login')->form([
            '_username' => 'user0@yopmail.com',
            '_password'  => '123',
        ]);
        $client->submit($form);
        // $this->assertResponseRedirects(); // boucle multiple redirect
        $client->followRedirect();
        $this->assertSelectorNotExists('.form-message-error');
    }
}