<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;

use App\Entity\User;

class UserTest extends TestCase
{
    public function testUserIsUser()
    {
        $user = new User();
        $this->assertInstanceOf(User::class, $user);
    }
}