<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail('user' . $i . '@yopmail.com');
            $user->setPassword('$2y$13$X3EsdH5oJdasKCGjey663O586OJriuh6iHkMW6U6qi56ZObPXxSGu');
            $manager->persist($user);
        }

        $manager->flush();
    }
}
