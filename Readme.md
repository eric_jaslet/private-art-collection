# Private art collection
private art collectio est un application permetant de gérer une collection d'art privée.

## Environement de developpement

### Pré-requis

* PHP 8.1.1
* Composer
* Docker
* Docker-compose
* nodejs et npm

### installer l'environement de developement
Créer un dossier pour le projet
Copier le dépot
A la racine du projet dossier parent déziper private_art_collection/docs/docker.zip

```bash
docker-compose up -d
docker exec -it www_docker_php8_symfony5 /bin/bash
make...............
```
voir pour faire un make dev :
    composer install
    apt-get install nodejs npm
    npm install
    npm run dev

## Compilation Js et scss

### Dev
```bash
docker exec -it www_docker_php8_symfony5 /bin/bash
cd private_art_collection/
npm run dev
```
### Prod
```bash
cd private_art_collection/
npm run build
```
## Lancer les fixtures
```bash
php bin/console doctrine:fixtures:load --env=test
```

## Lancer les tests
```bash
php bin/phpunit --testdox
```
